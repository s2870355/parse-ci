from pathlib import Path
import os
import subprocess
import sys
import time

from job_data import job_data

#I hate this but i work from visual studio code workspace so this fix is needed
dir_path = os.getcwd()

def get_all_sh(root_path) -> list[str]:
	result = []

	def recursive_search(folder_path):
		for item in folder_path.iterdir():
			try:
				if item.is_file() and item.suffix == '.sh':
					result.append(str(item.resolve()))
				elif item.is_dir() and 'venv' not in item.name:
					recursive_search(item)
			except:
				pass

	recursive_search(root_path)
	
	return result

def process_sh(files_sh : list[str]) -> list[job_data]:
	compile_info : list[job_data] = []
	for file in files_sh:
		try:
			info: job_data = job_data.construct_from_file(file)
		except FileNotFoundError:
			continue
		compile_info.append(info)
	return compile_info

def get_new_job(info : list[dict]) -> dict:
	for i in info:
		if len(i["depends"]) == 0:
			return i
	return None 

def run_cli_sequential(job : job_data) -> int:
	exec_location = os.path.join("/usr/src/project/",os.path.relpath(job.location , dir_path)).replace("\\", "/")
	disallowed = ['/', '\\', '-', '_', '.']
	logfile=  job.location.replace("\\", os.sep) + ".log"
	f = open(logfile, 'w')
	
	start_time = time.time()
	
	output = subprocess.call( \
		args= \
			[   "docker", \
				"run", \
				"--name", \
				f"{listToString([('_' if c in disallowed else c) for c in os.path.relpath(job.location)])}",  \
				"--rm", \
				"--net=host", \
				"-v", \
				f"{dir_path}:/usr/src/project", \
				"--entrypoint", "sh", \
				job.image, \
				"-c",
				f"REPO_DIR=/usr/src/project; export REPO_DIR; {exec_location}"],
				stdout=f,
				stderr=f)
	return None, f, start_time
def listToString(s):
 
	# initialize an empty string
	str1 = ""
 
	# traverse in the string
	for ele in s:
		str1 += ele
 
	# return string
	return str1

def run_cli_parallel(job : job_data) -> dict:
	exec_location = os.path.join("/usr/src/project/",os.path.relpath(job.location , dir_path)).replace(os.path.sep, "/")
	logfile=  job.location.replace("\\", os.sep) + ".log"
	f = open(logfile, 'w')
	disallowed = ['/', '\\', '-', '_', '.']
	start_time = time.time()
	task = subprocess.Popen( \
		args= \
			[   "docker", \
				"run", \
				"--name", \
				f"{listToString([('_' if c in disallowed else c) for c in os.path.relpath(job.location)])}",  \
				"--rm", \
				"--net=host", \
				"-v", \
				f"{dir_path}:/usr/src/project", \
				"--entrypoint", f"sh", \
				job.image,
				"-c",
				f"REPO_DIR=/usr/src/project; export REPO_DIR; {exec_location}"],
				stdout=f,
				stderr=f)
	output = f
	return (task, output, start_time)

def remove_from_depends(process_info : dict, jobname : str) -> dict:
	if jobname in process_info["depends"]:
		process_info["depends"].remove(jobname)
	return process_info

def can_run_job(compile_info : list[job_data], jobs):
	for j in compile_info:
		if j.can_run(compile_info):
			if j in [job[0] for job in jobs]:
				continue
			return True
	return False

def get_runnable_job(compile_info : list[job_data], jobs) -> job_data | None:
	for j in compile_info:
		if j.can_run(compile_info):
			if j in [job[0] for job in jobs]:
				continue
			return j
	return None
	

def run_ci(compile_info : list[job_data]) -> bool:
	jobs = []
	while can_run_job(compile_info, jobs) or len(jobs) != 0:
		change = False
		remove_able_jobs = []
		for job in jobs:
			(job_info,task, output, start_time) = job
			if task is None or task.poll() is not None:
				job_info.finished = True
				output.close()
				if task is None:
					job_info.failed = output
				else:
					job_info.failed = (task.returncode != 0)
				job_info.duration = time.time() - start_time
				change = True
				remove_able_jobs.append(job)
		for r_job in remove_able_jobs:
			jobs.remove(r_job)

		runnable_job = get_runnable_job(compile_info, jobs)
		if runnable_job is not None:
			print_info(compile_info, jobs, [runnable_job])
			if '--sequential' in sys.argv:
				(task, output, start_time) = run_cli_sequential(runnable_job)
			else:
				(task, output, start_time) = run_cli_parallel(runnable_job)
			jobs.append((runnable_job, task, output, start_time))
			change = True

		if change:
			print_info(compile_info, jobs)
	
	print("failed: ")
	for i in compile_info:
		if i.failed == True:
			print("\t" + i.location)
	def maxLengthOfLocation(array):
		maxLength = 0
		for obj in array:
			if (len(obj.location) > maxLength):
				maxLength = len(obj.location)
		return maxLength
	
	print('time info:')
	maxlength = maxLengthOfLocation(compile_info)
	for j in sorted(compile_info, key=lambda x: x.duration, reverse=True):
		print(f"\t{j.location.rjust(maxlength)}: {j.duration:.2f}")
	return True

def print_info(compile_info, jobs, additional=None):
	os.system("cls||clear")
	completed_count = 0
	failed_count = 0

	for j in compile_info:
		if j.finished == True:
			if j.failed == True:
				failed_count = failed_count + 1
			else:
				completed_count = completed_count + 1

	running_count = len(jobs) + (0 if additional is None else len(additional))
	waiting_count = len(compile_info) - failed_count - completed_count - running_count
	print("\U0001f7e9" * (completed_count) + "\U0001f7e5" * failed_count + "\U0001f7ea" * running_count + "\u2B1C" * waiting_count)
	print("current:")
	for job in jobs:
		(job_info,task, output, start_time) = job
		print("\t" + job_info.location)
	if additional is None:
		return
	for job in additional:
		print("\t" + job.location)

def get_stages(compile_info : list[job_data]) -> list[str]:
	stages = []
	for i in compile_info:
		if i.stage not in stages:
			stages.append(i.stage)
	return sorted(stages)

def add_to_dict(job_data, xdict) -> None:
	location = job_data.location
	parts = location.split('/')
	current_dict = xdict
	for part in parts[0:-1]:
		if part not in current_dict:
			current_dict[part] = {}
		current_dict = current_dict[part]
	current_dict[parts[-1]] = job_data

def list_to_dict(compile_info : list[job_data]) -> dict:
	output = {}
	for j in compile_info:
		add_to_dict(j, output)
	return output

def dict_to_nodes(dictionary) -> str:
	if isinstance(dictionary, dict):
		output_str = ""
		for k in sorted(dictionary.keys()):
			if isinstance(dictionary[k], dict):
				output_str += "\n\tsubgraph cluster_" + k.replace('-', '_') + "{" + "\n\t\tlabel=\"" + k +  "\"" 
				output_str += dict_to_nodes(dictionary[k]).replace('\n', "\n\t")
				output_str += "\n\t}"
			else:
				output_str += "\n\t" + dictionary[k].location.replace('/', "_").replace('\\', "_").replace("-", "_").replace(".", "_") + "[label=\"" + dictionary[k].location.split('/')[-1] + "\"];"
		return output_str
	return ""

def dict_to_edges(dictionary) -> str:
	if isinstance(dictionary, dict):
		output = ""
		for k in dictionary.keys():
			output += dict_to_edges(dictionary[k])
		return output
	if isinstance(dictionary, job_data):
		job = dictionary
		location = job.location
		depends = job.depends
		output_str = ""
		for depend in depends:
			d_location = depend
			output_str += "\t" + d_location.replace('/', "_").replace('\\', "_").replace("-", "_").replace(".", "_") + " -> " + location.replace('/', "_").replace('\\', "_").replace("-", "_").replace(".", "_") + ";\n"
		return output_str

def create_dot(compile_info : list[job_data]) -> None:
	dictionary = list_to_dict(compile_info)
	output = "digraph G {rankdir=\"LR\""
	output += dict_to_nodes(dictionary)
	output += '\n'
	output += dict_to_edges(dictionary)
	output += "}"
	return output

def generate_ci(compile_info : list[job_data]) -> str:
	output = ""
	stages = get_stages(compile_info)
	output = output + "stages: \n"
	for stage in stages:
		output = output + "\t- " + stage + '\n'
	for job in compile_info:
		try:
			output = output + str(job) + "\n"
		except Exception as e:
			print(job.location)
			print(e)
			pass
	return output

def pull_all(compile_info : list[job_data]):
	images = set([j.image for j in compile_info])
	downloads = [subprocess.Popen(["docker", "image", "pull", image]) for image in images]
	codes = [download.wait() for download in downloads]

folder = Path(dir_path)
if "--folder" in sys.argv:
	folder_param_index = sys.argv.index("--folder")
	if len(sys.argv) > folder_param_index:
		folder = Path(sys.argv[folder_param_index + 1])
info = process_sh(files_sh=get_all_sh(folder))
if "--pull" in sys.argv:
	pull_all(info)
	exit()
if "--static-dot" in sys.argv:
	print(create_dot(info))
	exit()
if "--ci" in sys.argv:
	with open(os.path.join(dir_path, "gitlab-ci-generated.yml"), 'w') as ci:
		ci.write(generate_ci(info).replace("\t", "    ").replace("\\", "\\\\"))
else:
	if not run_ci(compile_info=info):
		sys.exit(1)
from typing import Tuple
import os
import ast
import sys

class job_data:
	def __init__(self, location : str, image : str, depends : list[str], stage : str, artifact : list[str], cache : list[str], lock : str) -> None:
		self.location: str = os.path.relpath(location,os.getcwd()).replace("\\", '/')
		self.image: str = image
		self.depends: list[str] = depends
		self.stage: str = stage
		self.artifact: list[str] = artifact
		self.cache: list[str] = cache
		self.finished = False
		self.failed = False
		self.lock = lock
		self.duration = 0
		return
	
	def can_run(self, jobs : list["job_data"]) -> bool:
		if self.finished == True:
			return False
		dep = self.depends.copy()
		if len(dep) == 0:
			return True
		job_suc = 0
		for d in dep:
			for job in jobs:
				rel_path = os.path.relpath(job.location, os.getcwd())
				if rel_path != d:
					continue
				if job.finished == True:
					if job.failed == True:
						self.failed = True
						self.finished = True
					else:
						job_suc = job_suc + 1
		return len(dep) == job_suc
					
		
	@staticmethod
	def construct_from_string(file_path : str, file_string : str) -> "job_data":
		extracted_data = job_data.extract_data(file_path, file_string)
		if extracted_data is None:
			raise FileNotFoundError
		(image, depends, stage, artifact, cache, lock) = extracted_data
		return job_data(file_path, image, depends, stage, artifact, cache, lock)
			
	@staticmethod
	def construct_from_file(file_path : str) -> "job_data":
		with open(file=file_path, mode='r') as file:
			return job_data.construct_from_string(file_path, file.read())

	@staticmethod
	def normalize_path(file_path, depend) -> str:
		file_dir = os.path.dirname(file_path)
		dir_path = os.getcwd()
		file_location = os.path.join(file_dir, depend)
		relative_file = file_location.removeprefix(dir_path + os.path.sep)
		relative_file = os.path.relpath(relative_file)
		return relative_file
		
	@staticmethod
	def extract_data(file_path : str, file_string : str) -> None | Tuple[str, list[str], str, list[str], list[str], str]:
		if "parse-ci" in file_path:
			return None
		image = None
		depends = []
		stage = "unknown"
		artifacts = None
		cache = []
		lock = None
		for line in file_string.splitlines():
			line = line.removesuffix("\n")
			if not line.startswith("#"):
				break
			if line.startswith("#image: "):
				image = line.removeprefix("#image: ")
			if line.startswith("#stage: "):
				stage = line.removeprefix("#stage: ")
			if line.startswith("#depends: "):   
				text = line.removeprefix("#depends: ")    
				if "[]" in text:
					depends = []
					continue 
				if "[" in text:
					depends = [job_data.normalize_path(file_path, depend) for depend in ast.literal_eval(text)] 
					continue 
				depends = [job_data.normalize_path(file_path, text)] 
			if line.startswith("#image: "):
				image = line.removeprefix("#image: ")
			if line.startswith("#artifacts: "):    
				text = line.removeprefix("#artifacts: ")
				if "[]" in text:
					artifacts = []
					continue 
				if "[" in text:
					artifacts = [job_data.normalize_path(file_path, depend) for depend in ast.literal_eval(text)] 
					continue 
				artifacts = [job_data.normalize_path(file_path, text)] 
			if line.startswith("#cache: "):      
				text = line.removeprefix("#cache: ")   
				if "[]" in text:
					cache = []      
					continue 
				if "[" in text:
					cache = [job_data.normalize_path(file_path, depend) for depend in ast.literal_eval(text)] 
					continue 
				cache = [job_data.normalize_path(file_path, text)] 
			if line.startswith("#enable: false"):
				return None
			if line.startswith("#lock: "):
				lock = line.removeprefix("#lock: ")
		if image is None:
			if '--static-dot' in sys.argv:
				return None
			print("does not have a valid config")
			return None
		return (image, depends, stage, artifacts, cache, lock)
	
	def __str__(self) -> str:
		name : str = self.location.replace(os.path.sep, '/')
		stage : str = self.stage
		needs = [ r.replace(os.path.sep, '/') for r in self.depends]
		if self.artifact is None:
			artifact = "untracked"
		else:
			artifacts = self.artifact
		if len(self.cache) != 0:
			cache = self.cache
		else:
			cache = None
		image : str = self.image
		script : str = "REPO_DIR=$PWD; export REPO_DIR;\n\t\t- ./" + self.location
		output: str = "\n"
		output = output + name + ":\n"
		output = output + '\t' + "stage: " + stage + '\n'
		output = output + '\t' + "needs: " + str(needs) + '\n'
		output = output + '\t' + "image:\n\t\tname: " + image + "\n\t\tentrypoint: [\"\"]" + '\n'
		output = output + '\t' + 'script:\n\t\t- ' + script + '\n'
		if "--resource_group" in sys.argv:
			output = output + '\t' + "resource_group: lock" + '\n'
		else:
			if self.lock is not None:
				output = output + '\t' + f"resource_group: {self.lock}" + '\n'
		if artifacts == "untracked":
			output = output + '\t' + 'artifacts:\n\t\tuntracked: true\n'
		else:
			if len(artifacts) != 0: 
				output = output + '\t' + 'artifacts:\n\t\tpaths:\n'
				for artifact in artifacts:
					output = output + "\t\t\t- \"" + artifact + '\"\n'
		if cache is not None:
			if cache == "untracked":
				output = output + '\t' + 'cache:\n\t\tuntracked: true\n'
			else:
				output = output + '\t' + 'cache:\n\t\tpaths:\n'
				for c in cache:
					output = output + "\t\t\t- \"" + c + '\"\n'
		return output